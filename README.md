# Arch Linux on Asus X205TA

> Installation guide and tools for running Arch Linux on an Asus X205TA netbook.

After a recent inquiry, and and 8-year layoff, I've updated a few things on this project though it remains intact mostly for archival purposes -- hence the CONTRIBUTING page has been removed.

The X205TA netbook / laptop is a 10-year old device and had a complicated combination of architectures (32-bit and 64-bit).  The former is no longer supported by Arch Linux.

Additionally, the appropriate processes for creating a customized bootable ISO for Arch Linux have also changed in the intervening 8-years since this project was in active development.

With that said, this project should serve as a starting point and you will ultimately need to [RTFM](https://en.wikipedia.org/wiki/RTFM) and [DYOR](https://coinmarketcap.com/academy/glossary/dyor).

Good luck!

-----

# Preparation:

### 1) Secure Boot and Installation Media:

* Follow [instructions here](http://itsfoss.com/disable-uefi-secure-boot-in-windows-8/) 
* Find a USB thumb drive to use as the installation medium.
* Download installation media (ISO):
  * Option 1:  Hosted on Mega - [Archlinux-X205TA-2024-10](https://mega.nz/file/4rgxUbSL#Xyn322S2j1PsTvnzb5-d7SGXDPhqrfpN7UMLc074OnA)
  * Option 2:  Roll your own up-to-date version using one of the [scripts](https://gitlab.com/savagezen/x205ta/-/tree/master/scripts) I've created for such purposes.
  * *NOTE: These may not work or may need modified as I do not own this device anymore and cannot test content.  However, they should be enough to get you started.*

### 2a) Bootable USB (Windows):

* Download [Rufus](https://rufus.akeo.ie/) and use it to write the ISO to your USB with the following settings:
  * Partition schene for ```GPT / UEFI```
  * ```FAT32``` filesystem
  * ```ARCH_201606``` volume label (or corresponding to ISO version, ```ARCH_YYYYMM```)
  * Find your downloaded ISO file, and hit ```start```.

### 2b) Bootable USB (Linux / VM):

* Connect your USB device and locate it: ```$ lsblk```.
* Format the USB (e.g.  /dev/sdc): ```# # mkfs.vfat -F 32 -n ARCH_$(date +%Y%m) /dev/sdc```
  * *NOTE: the label of the device (ARCH_YYYYMM) has to match the label of the ISO .*
* Write the ISO to the USB: ```# dd bs=4M if=/path/to/archlinux-x205ta.sio of=/dev/sdc status=progress```

---

# Installation:

* After creating the live USB above:
  * connect it to your X205TA
  * reboot the X205TA and hammer ```F2``` to enter the BIOS
  * boot from the USB
* Follow the [Arch Installation Guide](https://wiki.archlinux.org/title/Installation_guide)
  * **Filesystem:** ```/boot``` will need to be a separate [partition](https://wiki.archlinux.org/title/Partitioning) formatted to ```FAT32``` in order to be compatible with X205TA's 32-bit bootloader.
  * **Bootloader:** From my previous notes, neither GRUB or Syslinux worked on a persistent installation, but [systemd-boot](https://wiki.archlinux.org/title/Systemd-boot) did.
* For machine specific hacks, mods, and work arounds, see:
  * [Arch Wiki Page: X205TA](https://wiki.archlinux.org/title/ASUS_x205ta)
  * [Arch Wiki Discussion](https://wiki.archlinux.org/title/Talk:ASUS_x205ta)

---

# Additional Resources:

* [Ifran's Blog](https://web.archive.org/web/20200803060417/https://ifranali.blogspot.com/2015/04/installing-arch-linux-on-asus-x205ta.html): The OG resource for getting this machine working.
* [Ubuntu Forums' Distor Agnostic Guide](https://ubuntuforums.org/showthread.php?t=2254322&p=13414345#post13414345): A collaborative discourse across Linux distributions for this machine.
* [Drivers and Tools](https://www.asus.com/us/supportonly/x205ta/helpdesk_download/): ASUS manufacturer's page