#!/usr/bin/sh
# create bootable Arch USB for Asus X205TA - ISO rebuild, old
# requires: grub, archiso
# preparation: USB must be partitioned and inserted.

ISO_VER=$(date +%Y.%m).01
ISO_SOURCE=archlinux-$ISO_VER-dual.iso
ARCH_VER=ARCH_$(date +%Y%m)
USB_DIR=/mnt
GRUB_CFG=https://gitlab.com/savagezen/x205ta/-/raw/master/firmware-linux/grub.cfg
KERNEL_SOURCE=https://mirrors.kernel.org/archlinux/iso
BRCM_BIN=https://gitlab.com/savagezen/x205ta/-/blob/master/firmware-android/fw_bcm43341.bin
BRCM_TXT=https://gitlab.com/savagezen/x205ta/-/blob/master/firmware-android/brcmfmac43340-sdio-x86_64.txt
ISO_CONFIG=/usr/share/archiso/configs/releng

# fetch stock iso
echo "Fetching stock ISO..."
curl -o $ISO_SOURCE $KERNEL_SOURCE//$ISO_VER/$ISO_SOURCE
mkdir -p /tmp/archiso
mount -o loop $ISO_SOURCE /tmp/archiso
cp -a archiso /tmp/customiso
umount /tmp/archiso
rm -r /tmp/archiso; rm $ISO_SOURCE

# modify iso
echo "Modifying ISO files..."
# fetch and update grub.cfg
curl -o /tmp/grub.cfg $GRUB_CFG
sed -i "s/ARCH_.*/$ARCH_VER/" /tmp/grub.cfg
# remake bootia32.efi
grub-mkstandalone -d /usr/lib/grub/i386-efi/ -O i386-efi --modules="part_gpt part_msdos" --fonts="unicode" --locales="uk" --themes="" -o "/tmp/customiso/EFI/BOOT/BOOTIA32.EFI" "boot/grub/grub.cfg=/tmp/grub.cfg" -v
# wireless drivers
cd /tmp/customiso/arch/x86_64
unsquashfs airootfs.sfs
curl -o /tmp/brcmfmac43340-sdio.bin $BRCM_BIN
curl -o /tmp/brcmfmac43340-sdio.txt $BRCM_TXT
zstd brcmfmac43340-sdio.bin -o squashfs-root/lib/firmware/brcm/brcmfmac43340-sdio.bin.zst
zstd brcmfmac43340-sdio.txt -o squashfs-root/lib/firmware/brcm/brcmfmac43340-sdio.ASUSTeK COMPUTER INC.-TF103CE.txt.zst
rm airootfs.sfs
mksquashfs squashfs-root airootfs.sfs
sha512sum airootfs.sfs > airootfs.sha512
rm -r squashfs-root

# generate new iso
echo "Generating new ISO image..."
cd /tmp
mkarchiso -v -w customiso $ISO_CONFIG
rm -r /tmp/customiso

# prepare usb
echo "Preparing to format USB..."
lsblk
echo -n "What deveice do you want to format and write to (/dev/sdXY): "
read TRG_DEV
mkfs.vfat F 32 -n $ARCH_VER $TRG_DEV
dd bs=4M if=/tmp/out/*.iso of=TRG_DEV status=progress
rm -r /tmp/out
