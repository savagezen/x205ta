#!/usr/bin/sh
# create bootable Arch USB for Asus X205TA, add manual boot file to ISO
# requires: grub
# preparation: USB must be inserted.

ISO_VER=$(date +%Y.%m).01
ISO_SOURCE=archlinux-$ISO_VER-dual.iso
KERNEL_SOURCE=https://mirrors.kernel.org/archlinux/iso
ARCH_VER=ARCH_$(date +%Y%m)
USB_DIR=/mnt
GRUB_CFG=https://gitlab.com/savagezen/x205ta/-/raw/master/firmware-linux/grub.cfg

# fetch stock iso
echo "Fetching Arch ISO..."
curl -o $ISO_SOURCE $KERNEL_SOURCE/$ISO_VER/$ISO_SOURCE

# prepare usb
echo "Preparing to format USB..."
lsblk
echo -n "What device do you want to format and write to (/dev/sdaXY): "
read TRG_DEV
mkfs.vfat -I -F 32 -n $ARCH_VER $TRG_DEV
mount $TRG_DEV $USB_DIR

# write stock ISO to disk as writable directory
echo "Writing ISO..."
bsdtar -x --exclude=isolinux/ --exclude=EFI/archiso/ --exclude=arch/boot/syslinux/ -f $ISO_SOURCE -C $USB_DIR

# pull and build firmware
echo "Fetching firmware..."
curl -o /tmp/grub.cfg $GRUB_CFG			# pull grub.cfg
sed -i "s/ARCH_.*/$ARCH_VER/" /tmp/grub.cfg	# update Arch Linux version
grub-mkstandalone -d /usr/lib/grub/i386-eif/ -O i386-efi --modules=part_gpt part_msdos" --fonts="unicode" --locales="uk" --themes="" -o "$USB_DIR/EFI/BOOT/BOOTIA32.EFI" "boot/grub/grub.cfg=/tmp/grub.cfg" -v	# compile new bootia
umount $USB_DIR

# reference:
# Wireless drivers should work out of the box.
# If they do not, consult the Arch Linux Wiki.
#
# After initial boot, you may need to further
# edit the wireless drivers on the newly installed system.
# e.g.
# $ cp /sys/firmware/efi/efivars/nvram<TAB> /lib/firmware/brcm/brcmfmac43340-sdio.txt
# $ rmmod brcmfmac
# $ modprobe brcmfmac
